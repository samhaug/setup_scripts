#!/bin/sh

mkdir -p $HOME/.local/share

./configure CC=mpiicc CXX=mpiicc FC=mpiifort MPICC=mpiicc MPICXX=mpiicc\
     MPIFC=mpiifort LIBS=-lpthread --prefix=$HOME/.local/share

