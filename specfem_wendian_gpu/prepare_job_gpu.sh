#!/bin/sh

# Run this in Specfem Folder

root_dir=/scratch/$USER
name=$1

if [[ -z $name ]]; then
    echo "No job name is given."
    exit
fi

target_dir=${root_dir}/$name

if [[ -d $target_dir ]]; then
    echo "Folder exists!"
    read -p "Do you want to delete it (y/n)? " answer
    case ${answer:0:1} in
	y|Y )
            echo Deleting previous folder...
	    rm -rf $target_dir
	    ;;
	* )
            echo Exiting...
	    exit
	    ;;
    esac
fi

CURDIR=`pwd`

mkdir -p $target_dir
cd $target_dir
mkdir -p DATA DATABASES_MPI OUTPUT_FILES
cp -r $CURDIR/bin .
cd DATA
echo "Copying parameter files..."
cp -r $CURDIR/DATA/{Par_file,STATIONS,CMTSOLUTION} .
echo "Linking model files..."
ln -s $CURDIR/DATA/cemRequest .
ln -s $CURDIR/DATA/crust1.0 .
ln -s $CURDIR/DATA/crust2.0 .
ln -s $CURDIR/DATA/crustmap .
ln -s $CURDIR/DATA/epcrust .
ln -s $CURDIR/DATA/eucrust-07 .
ln -s $CURDIR/DATA/full_sphericalharmonic_model .
ln -s $CURDIR/DATA/heterogen .
ln -s $CURDIR/DATA/Lebedev_sea99 .
ln -s $CURDIR/DATA/Montagner_model . 
ln -s $CURDIR/DATA/old .
ln -s $CURDIR/DATA/PPM .
ln -s $CURDIR/DATA/QRFSI12 .
ln -s $CURDIR/DATA/s20rts .
ln -s $CURDIR/DATA/s362ani .
ln -s $CURDIR/DATA/s40rts .
ln -s $CURDIR/DATA/sglobe .
ln -s $CURDIR/DATA/Simons_model .
ln -s $CURDIR/DATA/topo_bathy .
ln -s $CURDIR/DATA/Zhao_JP_model .

#ln -s $HOME/DATA/GLL_M15 GLL

echo
nproc_xi=`grep NPROC_XI Par_file | cut -d "=" -f 2`
nproc_eta=`grep NPROC_ETA Par_file | cut -d "=" -f 2`
nchunks=`grep NCHUNKS Par_file | cut -d "=" -f 2`
NPROC=$((nproc_xi*nproc_eta*nchunks))
echo NPROC=$NPROC
NUMBER_OF_NODES=`python2 -c "import math; print int(math.ceil($NPROC/20.0))"`
echo nodes=$NUMBER_OF_NODES

cd ..

cp $CURDIR/{change_simulation_type.pl,specfem_wendian_job_gpu.sh} .
sed -i "s/:NUMBER_OF_NODES:/$NUMBER_OF_NODES/g" specfem_wendian_job_gpu.sh
sed -i "s/:NPROC:/$NPROC/g" specfem_wendian_job_gpu.sh
sed -i "s/:NAME:/$name/g" specfem_wendian_job_gpu.sh


echo You can submit the job using:
echo cd $target_dir
echo sbatch $target_dir/specfem_wendian_job.sh
