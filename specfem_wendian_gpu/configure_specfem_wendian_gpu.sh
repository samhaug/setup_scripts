#!/bin/sh

# Compile with ADIOS using Intel Compilers

ASDF=/sw/libs/asdf-library/1.0.2-intel-impi/lib64/libasdf.a
ADIOS=/u/eu/de/haugland/.local/share/lib/
CUDA=/sw/libs/cuda/10.1/lib64/
MPI=/sw/compilers/intel/2019/compilers_and_libraries_2019.3.199/linux/mpi/intel64/include

./configure  FC=mpiifort CC=mpiicc\
       MPIFC=mpiifort MPICC=mpiicc CFLAGS="-g -O3" FCFLAGS="-g -O3" \
	     ASDF_LIBS=$ASDF CUDA_LIB=$CUDA MPI_INC=$MPI \
       ADIOS_LIBS=$ADIOS --with-adios --with-asdf --with-cuda=cuda9
       
#Currently Loaded Modules:
  #1) libs/szip/2.1.1-gnu    3) mpi/impi/2019-intel           5) libs/boost/1.68.0-intel-impi         7) libs/cuda/10.1
  #2) compilers/intel/2019   4) libs/hdf5/1.8.10-intel-impi   6) libs/asdf-library/1.0.2-intel-impi

