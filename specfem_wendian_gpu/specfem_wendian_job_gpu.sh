#!/bin/bash -x
#SBATCH --job-name=:NAME:
#SBATCH --exclusive
#SBATCH --export=ALL
#SBATCH --nodes=:NUMBER_OF_NODES:
#SBATCH --ntasks-per-node=20
#SBATCH --mem=190000
#SBATCH --time=02:00:00
#SBATCH -p gpu


NPROC=:NPROC:
cd $SLURM_SUBMIT_DIR
./change_simulation_type.pl -f
mpirun -np $NPROC ./bin/xmeshfem3D
#./change_simulation_type.pl -F
mpirun -np $NPROC ./bin/xspecfem3D

#SBATCH --gres=gpu:v100::NUMBER_OF_NODES:
